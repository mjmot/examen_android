DELIMITER //
CREATE PROCEDURE getAllCountries()
BEGIN
    SELECT id, name_en FROM countries;
END;
//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE getCountryById(IN country_id INT)
BEGIN
    SELECT id, name_en FROM countries WHERE id = country_id;
END;
//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE getCountryByName(IN country_name VARCHAR(255))
BEGIN
    SELECT id, name_en FROM countries WHERE name_en = country_name;
END;
//
DELIMITER ;