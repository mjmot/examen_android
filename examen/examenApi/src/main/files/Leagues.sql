DELIMITER //
CREATE PROCEDURE insertLeague(
    IN new_strCountry VARCHAR(255),
    IN new_strFanart1 VARCHAR(255),
    IN new_strFanart2 VARCHAR(255),
    IN new_strFanart3 VARCHAR(255),
    IN new_strFanart4 VARCHAR(255),
    IN new_strDescriptionEN TEXT,
    IN new_strLeague VARCHAR(255),
    IN new_strBadge VARCHAR(255),
    IN new_strWebsite VARCHAR(255)
)
BEGIN
   INSERT INTO leagues (strCountry, strFanart1, strFanart2, strFanart3, strFanart4, strDescriptionEN, strLeague, strBadge, strWebsite)
   VALUES (new_strCountry, new_strFanart1, new_strFanart2, new_strFanart3, new_strFanart4, new_strDescriptionEN, new_strLeague, new_strBadge, new_strWebsite);
END;
//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE getAllLeaguesByCountry(IN country VARCHAR(255))
BEGIN
    SELECT * FROM leagues where strCountry = country;
END;
//
DELIMITER ;