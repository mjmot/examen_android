drop schema if exists examen;
create schema examen;

use examen;

CREATE TABLE countries (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name_en VARCHAR(255) NOT NULL
);

INSERT INTO countries (name_en) VALUES
('United Arab Emirates'),
('Australia'),
('Chile'),
('China'),
('Colombia'),
('Costa Rica'),
('Cuba'),
('England'),
('Spain'),
('Ethiopia'),
('Europe'),
('Finland'),
('Hong Kong'),
('Montenegro'),
('Malta'),
('Swaziland'),
('Thailand'),
('Tunisia'),
('Turkey'),
('Taiwan'),
('United States'),
('Uruguay'),
('Venezuela'),
('Worldwide'),
('Wales'),
('South Africa');

CREATE TABLE leagues (
    strCountry VARCHAR(255),
    strFanart1 VARCHAR(255),
    strFanart2 VARCHAR(255),
    strFanart3 VARCHAR(255),
    strFanart4 VARCHAR(255),
    strDescriptionEN TEXT,
    strLeague VARCHAR(255),
    strBadge VARCHAR(255),
    strWebsite VARCHAR(255)
);
