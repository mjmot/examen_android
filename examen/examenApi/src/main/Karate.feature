Feature: User API Tests

Background:
    * def country =
    """
    {
      "id": #number,
      "name_en": '#string?'
    }
    """

    * def league =
    """
    {
      "strCountry": "#string",
      "strFanart1": "#string",
      "strFanart2": "#string",
      "strFanart3": "#string",
      "strFanart4": "#string",
      "strDescriptionEN": "#string",
      "strLeague": "#string",
      "strBadge": "#string",
      "strWebsite": "#string"
    }
    """


Scenario: Get hello world
    Given url 'http://localhost:8080/helloWorld'
    When method GET
    Then status 200
    And match response == 'Hello World!'

Scenario: Get countries
    Given url 'http://localhost:8080/countries'
    When method GET
    Then status 200
    And match each response == country

Scenario: Get country by id
    Given url 'http://localhost:8080/countries/1'
    When method GET
    Then status 200
    And match response == country
    And def expectedResponse = read('../responses/GetContryId_1.json')
    And match response == expectedResponse

Scenario: Get country by name
    Given url 'http://localhost:8080/country?name=Spain'
    When method GET
    Then status 200
    And match response == country
    And def expectedResponse = read('../responses/GetContryName_spain.json')
    And match response == expectedResponse

Scenario: Insert League json
    Given url 'http://localhost:8080/country?name=Spain'
    When method GET
    Then status 200
    And match response == country
    And def expectedResponse = read('../responses/GetContryName_spain.json')
    And match response == expectedResponse

Scenario: Insert League from JSON file
    Given url 'http://localhost:8080/leagues'
    And request read('../responses/PostLeagues.json')
    When method POST
    Then status 200


Scenario: Get Leagues
    Given url 'http://localhost:8080/leagues?name=XX'
    When method GET
    Then status 200
    And match each response == league
