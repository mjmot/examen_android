package com.example.demo.dao;

import com.example.demo.model.Country;
import com.example.demo.model.League;
import com.example.demo.utils.CountryConstants;
import com.example.demo.utils.LeaguesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LeagueRepository {
    private final JdbcTemplate JDBC_TEMPLATE;

    @Autowired
    public LeagueRepository(JdbcTemplate JDBC_TEMPLATE) {
        this.JDBC_TEMPLATE = JDBC_TEMPLATE;
    }


    public void insertLeague(String strCountry, String strFanart1, String strFanart2, String strFanart3, String strFanart4, String strDescriptionEN, String strLeague, String strBadge, String strWebsite) {
        JDBC_TEMPLATE.update(LeaguesConstants.CREATE_LEAGUE, strCountry, strFanart1, strFanart2, strFanart3, strFanart4, strDescriptionEN, strLeague, strBadge, strWebsite);
    }

    public List<League> getAllLeagues(String country) {
        return JDBC_TEMPLATE.query(LeaguesConstants.GET_ALL_LEAGUES_BY_COUNTRY, new BeanPropertyRowMapper<>(League.class), country);
    }
}
