package com.example.demo.api;

import com.example.demo.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {
    @Autowired
    private CountryService service;

    @GetMapping("/helloWorld")
    public ResponseEntity getHelloWorld() {
        return service.getHelloWorld();
    }

    @GetMapping("/countries")
    public ResponseEntity getCountries() {
        return service.getCountries();
    }

    @GetMapping("/countries/{id}")
    public ResponseEntity getCountriesById(@PathVariable("id") int id) {
        return service.getCountriesById(id);
    }

    @GetMapping("/country")
    public ResponseEntity getCountryByName(@RequestParam String name) {
        return service.getCountryByName(name);
    }
}
