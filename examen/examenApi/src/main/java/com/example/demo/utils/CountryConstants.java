package com.example.demo.utils;

public class CountryConstants {
    public static final String GET_ALL_COUNTRIES = "call getAllCountries()";
    public static final String GET_COUNTRY_BY_ID = "call getCountryById(?)";
    public static final String GET_COUNTRY_BY_NAME = "call getCountryByName(?)";
}
