package com.example.demo.model;

import lombok.Data;

import java.util.ArrayList;

@Data
public class LeagueRequest {
    private ArrayList<League> countries;
}
