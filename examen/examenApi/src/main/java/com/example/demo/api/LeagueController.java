package com.example.demo.api;

import com.example.demo.model.League;
import com.example.demo.model.LeagueRequest;
import com.example.demo.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LeagueController {
    @Autowired
    private LeagueService service;

    @PostMapping("/leagues")
    public ResponseEntity postLeagues(@RequestBody LeagueRequest leagues) {
        return service.postLeagues(leagues.getCountries());
    }

    @GetMapping("/leagues")
    public ResponseEntity getAllLeagues(@RequestParam String name) {
        return service.getAllLeagues(name);
    }
}
