package com.example.demo.service;

import com.example.demo.dao.CountryRepository;
import com.example.demo.dao.LeagueRepository;
import com.example.demo.model.Country;
import com.example.demo.model.League;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LeagueService {
    @Autowired
    private LeagueRepository repository;

    public ResponseEntity postLeagues(ArrayList<League> leagues) {
        for (League league : leagues) {
            repository.insertLeague(league.getStrCountry(), league.getStrFanart1(), league.getStrFanart2(), league.getStrFanart3(), league.getStrFanart4(), league.getStrDescriptionEN(), league.getStrLeague(), league.getStrBadge(), league.getStrWebsite());
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    public ResponseEntity getAllLeagues(String country) {
        List<League> leagues = repository.getAllLeagues(country);
        if (leagues == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        if (leagues.size() < 1) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(leagues);
    }
}
