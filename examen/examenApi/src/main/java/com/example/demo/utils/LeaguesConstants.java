package com.example.demo.utils;

public class LeaguesConstants {
    public static final String CREATE_LEAGUE = "call insertLeague(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    public static final String GET_ALL_LEAGUES_BY_COUNTRY = "call getAllLeaguesByCountry(?)";
}
