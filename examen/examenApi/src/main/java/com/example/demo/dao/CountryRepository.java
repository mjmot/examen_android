package com.example.demo.dao;

import com.example.demo.model.Country;
import com.example.demo.utils.CountryConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepository {
    private final JdbcTemplate JDBC_TEMPLATE;

    @Autowired
    public CountryRepository(JdbcTemplate JDBC_TEMPLATE) {
        this.JDBC_TEMPLATE = JDBC_TEMPLATE;
    }

    public List<Country> getAllCountries() {
        return JDBC_TEMPLATE.query(CountryConstants.GET_ALL_COUNTRIES, new BeanPropertyRowMapper<>(Country.class));
    }

    public Country getCountryById(int id) {
        return JDBC_TEMPLATE.queryForObject(CountryConstants.GET_COUNTRY_BY_ID, new BeanPropertyRowMapper<>(Country.class), id);
    }

    public Country getCountryByName(String name) {
        return JDBC_TEMPLATE.queryForObject(CountryConstants.GET_COUNTRY_BY_NAME, new BeanPropertyRowMapper<>(Country.class), name);
    }
}
