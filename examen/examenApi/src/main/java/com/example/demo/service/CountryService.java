package com.example.demo.service;

import com.example.demo.dao.CountryRepository;
import com.example.demo.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryService {
    @Autowired
    private CountryRepository repository;

    public ResponseEntity getHelloWorld() {
        return ResponseEntity.status(HttpStatus.OK).body("Hello World!");
    }

    public ResponseEntity getCountries() {
        List<Country> countries = repository.getAllCountries();
        if (countries == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        if (countries.size() < 1) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(countries);
    }

    public ResponseEntity getCountriesById(int id) {
        Country country = repository.getCountryById(id);
        if (country != null) {
            return ResponseEntity.status(HttpStatus.OK).body(country);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity getCountryByName(String name) {
        Country country = repository.getCountryByName(name);
        if (country != null) {
            return ResponseEntity.status(HttpStatus.OK).body(country);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
