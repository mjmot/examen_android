package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class League {
    private String strCountry;
    private String strFanart1;
    private String strFanart2;
    private String strFanart3;
    private String strFanart4;
    private String strDescriptionEN;
    private String strLeague;
    private String strBadge;
    private String strWebsite;

    public League(){}
}
