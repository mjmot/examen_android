package com.example.examenapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.examenapp.model.Country;
import com.example.examenapp.model.League;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<League> leagues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        leagues = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);

        String country = getIntent().getStringExtra("countryName");
        getLeagues(country);
    }

    private void getLeagues(String country) {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                //Constants.BASE_URL + "/leagues?name=" + country,
                "https://www.thesportsdb.com/api/v1/json/3/search_all_leagues.php?c=" + country,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray responseArray = response.getJSONArray("countries");
                            for (int i = 0; i < responseArray.length(); i++) {
                                JSONObject obj = responseArray.getJSONObject(i);
                                League league = new League();
                                league.setStrBadge(obj.getString("strBadge"));
                                league.setStrLeague(obj.getString("strLeague"));
                                league.setStrDescriptionEN(obj.getString("strDescriptionEN"));
                                league.setStrCountry(obj.getString("strCountry"));
                                league.setStrFanart1(obj.getString("strFanart1"));
                                league.setStrFanart2(obj.getString("strFanart2"));
                                league.setStrFanart3(obj.getString("strFanart3"));
                                league.setStrFanart4(obj.getString("strFanart4"));
                                league.setStrWebsite(obj.getString("strWebsite"));
                                leagues.add(league);
                            }
                            recyclerView.setAdapter(new AdapterLeagues(leagues, DetailActivity.this));
                            recyclerView.setLayoutManager(new LinearLayoutManager(DetailActivity.this));
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, "Error: " + error, Toast.LENGTH_SHORT).show();
                    }
                }
        );
        queue.add(jsonObjectRequest);
    }
}