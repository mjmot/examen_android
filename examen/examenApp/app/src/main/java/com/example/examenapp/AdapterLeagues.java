package com.example.examenapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examenapp.model.League;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterLeagues extends RecyclerView.Adapter<AdapterLeagues.ViewHolder> {
    private List<League> leagues;
    private Context context;

    public AdapterLeagues(List<League> leagues, Context context) {
        this.leagues = leagues;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterLeagues.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.league_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterLeagues.ViewHolder holder, int position) {
        League league = leagues.get(position);
        Picasso.get().load(league.getStrBadge())
                        .fit()
                        .centerCrop()
                        .into(holder.imageView);
        holder.textTitle.setText(league.getStrLeague());
        holder.textDescription.setText(league.getStrDescriptionEN());
        holder.buttonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra("web", league.getStrWebsite());
                context.startActivity(intent);
            }
        });

        holder.buttonImage.setAlpha(!league.getStrFanart1().equals("") || !league.getStrFanart1().equals(null) ? 1 : 0);
        holder.buttonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] images = {league.getStrFanart1(), league.getStrFanart2(), league.getStrFanart3(), league.getStrFanart4()};
                Intent intent = new Intent(context, ImagesActivity.class);
                intent.putExtra("images", images);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return leagues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textTitle;
        private TextView textDescription;
        private Button buttonWeb;
        private Button buttonImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textTitle = itemView.findViewById(R.id.textTitle);
            textDescription = itemView.findViewById(R.id.textDescription);
            buttonWeb = itemView.findViewById(R.id.button);
            buttonImage = itemView.findViewById(R.id.button2);
        }
    }
}
