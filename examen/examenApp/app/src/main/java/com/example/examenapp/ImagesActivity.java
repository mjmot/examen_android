package com.example.examenapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

public class ImagesActivity extends AppCompatActivity {
    private ViewPager2 viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        viewPager = findViewById(R.id.viewPager);
        String[] images = getIntent().getStringArrayExtra("images");
        viewPager.setAdapter(new ImageAdapter(this, images));
    }
}