package com.example.examenapp.model;

public class Country {
    private int id;
    private String name_en;

    public Country(){}

    public Country(int id, String name_en) {
        this.id = id;
        this.name_en = name_en;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }
}
